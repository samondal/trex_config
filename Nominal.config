% Config file corresponds to the total data
% author - Santu Mondal(santu.mondal@cern.ch)
% ttbar_ljets fit
% Check the Luminosity syst block !!! Important

% --------------- %
% ---  JOB    --- %
% --------------- %

Job: Data_ttbar_ljets_All212242_AntiKt4HI_07_07_2023_paper_draft_results_wo_fake_syst
  Label: "Data Fit"
  AtlasLabel: Internal
  CmeLabel: 8.16 TeV
  POI: mu_XS_ttbar
  InputFolder: ttbar_ljets_Data/
  InputName: ttbar
  ReadFrom: NTUP
  NtuplePath: "/eos/user/s/samondal/sample_pre_approval_HION"
  NtupleName: "events" 
  LumiLabel: 164.6 nb^{-1}
  Lumi: XXX_LUMI % pb^-1	
  PlotOptions: PREFITONPOSTFIT,NOXERR,YIELDS
  DebugLevel: 2
  HistoChecks: NOCRASH
% SplitHistoFile: TRUE % use for ntuples
  MergeUnderOverFlow: TRUE
  ImageFormat: png,eps,pdf,C
%  MCstatThreshold: 0.01
  MCstatConstraint: Poisson
  GetChi2: TRUE 
  SystControlPlots: TRUE %Use TRUE at final run, %FALSE to speedup
  SystErrorBars: TRUE
%  SystPruningShape: 0.005
%  SystPruningNorm: 0.005
%  SystLarge: 1
  CorrelationThreshold: 0.10
  SystCategoryTables: TRUE
  RankingMaxNP: 25
  DoSummaryPlot: TRUE  
%  SummaryPlotYmin: 0.05 
  DoTables: TRUE
  DoPieChartPlot: TRUE
  ReplacementFile: ttbar_ljets_Data/replacement_85_merged.txt
  UseGammasForCorr: TRUE % It will include gammas into the plots 
  UseGammaPulls: TRUE % It will plot the gammas as nuis parameters
%  StatOnly: TRUE  
%  UseATLASRounding: TRUE
  UseATLASRoundingTxt: TRUE
  UseATLASRoundingTex: TRUE     

% --------------- %
% ----- FIT ----- %
% --------------- %

Fit: fit
  FitType: SPLUSB
  FitRegion: CRSR
  FitBlind: FALSE  % True when Asimov
  POIAsimov: 1  % clarify !!
  UseMinos: mu_XS_ttbar
  BlindedParameters: mu_XS_ttbar
%  SetRandomInitialNPval: 0.1
%  SetRandomInitialNPvalSeed: 1234567

% --------------- %
% ---- LIMIT ---- %
% --------------- %

Limit: limit
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
%  SignalInjection: TRUE
%  SignalInjectionValue: XXX

% --------------- %
% POI FLOAT NORMS %
% --------------- %

 NormFactor: mu_XS_ttbar
   Title: "#mu (ttbar)"
   Min: -100
   Max: 100
   Nominal: 1
   Samples: mc-ttbar  %put all Signal samples by comma 
   Constant: FALSE

% --------------- %
% LUMINOSITY SYST %
% --------------- %

Systematic: lumi
  Title: Luminosity
  Type: OVERALL
  OverallUp: 0.024
  OverallDown: -0.024
  Category: Luminosity
%  SubCategory: Luminosity
%  Exclude: XXX_EXCLUDE_DATA_DRIVEN

%put the luminosity corresponds to 165 nb 

% --------------- %
% --- REGIONS --- %
% --------------- %

% -------------------
% l+jets regions:
% -------------------

Region: "HTjl_4j1b1l_ejets"
  Label: "4j1b1l_ejets"
  Type: SIGNAL
  VariableTitle: "HTjl"
  Variable: "HTjl", 7, 100, 1000 % use for ntuples
  Selection: "HTjl>0 && (nbjets==1) && channel==0"
  Binning: 100, 200, 300, 400, 500, 600, 700, 1000

Region: "HTjl_4j1b1l_mujets"
  Label: "4j1b1l_mujets"
  Type: SIGNAL
  VariableTitle: "HTjl"
  Variable: "HTjl", 7, 100, 1000 % use for ntuples
  Selection: "HTjl>0 && (nbjets==1) && channel==1"
  Binning: 100, 200, 300, 400, 500, 600, 700, 1000

Region: "HTjl_4j2bincl1l_ejets"
  Label: "4j2bincl1l_ejets"
  Type: SIGNAL
  VariableTitle: "HTjl"
  Variable: "HTjl", 7, 100, 1000 %use for ntuples
  Selection: "HTjl>0 && (nbjets>=2) && channel==0"
  Binning: 100, 200, 300, 400, 500, 600, 700, 1000

Region: "HTjl_4j2bincl1l_mujets"
  Label: "4j2bincl1l_mujets"
  Type: SIGNAL
  VariableTitle: "HTjl"
  Variable: "HTjl", 7, 100, 1000 %use for ntuples
  Selection: "HTjl>0 && (nbjets>=2) && channel==1"
  Binning: 100, 200, 300, 400, 500, 600, 700, 1000


% -------------------
% HTjl Dilep test regions:
% -------------------

Region: "HTjl_2j1b2l"
  Label: "2j1b2l"
  Type: SIGNAL
  VariableTitle: "HTjl"
  Variable: "HTjl", 5, 100, 1000 % use for ntuples
%  Selection: "HTjl>0 && (nbjets==1) && channel>1 && beam==1"   %       0 or 1, standing for pPb and Pbp
  Selection: "HTjl>0 && (nbjets==1) && channel>1"
  Binning: 100, 200, 300, 400, 500, 1000
%  Binning: 0.8,0.91,1 %please reconfirm binning & GeV % for ntuples
%  LogScale: True %

Region: "HTjl_2j2bincl2l"
  Label: "2j2bincl2l"
  Type: SIGNAL
  VariableTitle: "HTjl"
  Variable: "HTjl", 5, 100, 1000  % use for ntuples
%  Selection: "HTjl>0 && (nbjets>=2) && channel>1 && beam==1"   %       0 or 1, standing for pPb and Pbp
  Selection: "HTjl>0 && (nbjets>=2) && channel>1"
  Binning: 100, 200, 300, 400, 500, 1000
%  Binning: 0.8,0.91,1 %please reconfirm binning & GeV % for ntuples
%  LogScale: True %

% --------------- %
% Reff PDF SAMPLE %
% --------------- %

Sample: "ttbar_PDF_PDF4LHC"
  Type: GHOST
  NtupleFiles: histos-2016-mc-ttbar_All212242-AntiKt4HI-newR21-NEW_MUR1_MUF1_PDF90900
  MCweight: XXX_LJETS_MCWEIGHT

% --------------- %
% - MC GHOST    - %
% --------------- %

Sample: "mc-ttbar_Inclusive"
  Type: GHOST
  Title: "t#bar{t}_Inclusive"
  NtupleFiles: XXX_ttbar_All_samples
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

% --------------- %
% - DATA SAMPLE - %
% --------------- %

Sample: Data
  Type: DATA
  Title: Data
  NtupleFiles: XXX_Data_samples
  FillColor: 1
  LineColor: 1

% --------------- %
% - MC SAMPLES  - %
% --------------- %

Sample: "mc-ttbar"
  Type: SIGNAL
  Title: "t#bar{t}"
%  Group: Signal
  FillColor: 633
  LineColor: 1
  NtupleFiles: XXX_ttbar_All_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

Sample: "mc-tchan"
  Type: BACKGROUND
  Title: "tchan"
  Group: single top
  FillColor: 797
  LineColor: 1
  NtupleFiles: XXX_tchan_All_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

Sample: "mc-vj-W_BFilter"
  Type: BACKGROUND
  Title: "Wl\nu b"
%  Group: W
  FillColor: 418
  LineColor: 1
  NtupleFiles: XXX_W_B_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"
  Exclude: HTjl_2j1b2l, HTjl_2j2bincl2l

Sample: "mc-vj-W_CFilterBVeto"
  Type: BACKGROUND
  Title: "Wl\nu c"
%  Group: W
  FillColor: 417
  LineColor: 1
  NtupleFiles: XXX_W_C_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"
  Exclude: HTjl_2j1b2l, HTjl_2j2bincl2l
  
Sample: "mc-vj-W_CVetoBVeto"
  Type: BACKGROUND
  Title: "Wl\nu light"
%  Group: W
  FillColor: 416
  LineColor: 1
  NtupleFiles: XXX_W_Light_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"
  Exclude: HTjl_2j1b2l, HTjl_2j2bincl2l

Sample: "mc-vj-Z_BFilter"
  Type: BACKGROUND
  Title: "Zll b"
%  Group: Z
  FillColor: 855
  LineColor: 1
  NtupleFiles: XXX_Z_B_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"


Sample: "mc-vj-Z_CFilterBVeto"
  Type: BACKGROUND
  Title: "Zll c"
%  Group: Z
  FillColor: 861
  LineColor: 1
  NtupleFiles: XXX_Z_C_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"


Sample: "mc-vj-Z_CVetoBVeto"
  Type: BACKGROUND
  Title: "Zll light"
%  Group: Z
  FillColor: 859
  LineColor: 1
  NtupleFiles: XXX_Z_Light_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

Sample: "mc-vv"
  Type: BACKGROUND
  Title: "diboson"
  FillColor: 920
  LineColor: 1
  NtupleFiles: XXX_vv_All_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

Sample: "mc-wt"
  Type: BACKGROUND
  Title: "wt_All"
  Group: single top
  FillColor: 432
  LineColor: 797
  NtupleFiles: XXX_wt_All_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

Sample: "fakes"
  Type: BACKGROUND
  Title: "Fakes"
  FillColor: 618
  LineColor: 1
  NtupleFiles: XXX_fakes_samples
  Selection: XXX_LJETS_SELECTION
  MCweight: XXX_LJETS_MCWEIGHT
  %Regions: "HTjl_4j1b1l", "HTjl_4j2bincl1l", "HTjl_2j1b2l", "HTjl_2j2bincl2l"

